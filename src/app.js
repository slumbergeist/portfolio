const mainView = document.querySelector('body');
const navBar = $('.site-nav');
const viewPort = $('html,body');

const sections = $($('.site-section').get().reverse()); // cache all sections
const navLinks = $('.site-nav > ul > li > a');		// cache all navigational links
const sectionNavMap = {};	//map of navigational links to corresponding section

sections.each(function() {
	sectionNavMap[$(this).attr('id')] = $(`.site-nav > ul > li > a[href=\\#${$(this).attr('id')}]`);
})

// tracks last scroll position
let lastScrollPosition = 0;

const changeNavBg = (activeLink) => {
	navBar.css({'background-color':'#1f895c'});
}
// update active navigation link according to visible section
const updateActiveSectionLink = () => {
	sections.each(function () {
		try {
			const current = $(this);
			const top = $(this).offset().top ;
			const id = current.attr('id');
			const link = sectionNavMap[id];

			setNavColor(id, link);
			
			if (lastScrollPosition >= top - 100) {
				if (!link.hasClass('active')) {
					navLinks.removeClass('active');
					if(id !== 'banner-section'){
						link.addClass('active');
					}
				}
				return false;
			}
		} catch(error) {
			console.error(error);
		}
	});
}

// utility callback function for scroll throttling
const scrollHandle = function() {
	lastScrollPosition = $(this).scrollTop();
	// fixNavigation(lastScrollPosition);
	updateActiveSectionLink();
}

// scroll event listener
$(window).scroll(_.throttle(scrollHandle, 160));

navLinks.on('click', function (event) {
	event.preventDefault();
	viewPort.animate({
		scrollTop: $($(this.hash)).offset().top
	}, 500);
});

$('.top-logo').on('click', function(event) {
	event.preventDefault();
	viewPort.animate({
		scrollTop: $($(this.hash)).offset().top
	}, 500);
})

function setNavColor(id, link) {
	if((lastScrollPosition > 100 || $(window).scrollTop() > 100) && !sectionNavMap['banner-section'].hasClass('color')){
		navBar.addClass('color');
	}
	else if((lastScrollPosition < 100 || $(window).scrollTop() < 100) ){
		navBar.removeClass('color');
	}
}

updateActiveSectionLink();

$('.galore').each(function () {
	var $wrapper = $(this);
	var img = $wrapper.find('img')[0];

	var tempImg = new Image();
	tempImg.src = img.src;
	tempImg.onload = function () {
		$wrapper.removeClass('loading');
		$wrapper.addClass('loaded');
	};
})
