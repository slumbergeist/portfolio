const gulp = require('gulp');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
const util = require('gulp-util');
const htmlmin = require('gulp-htmlmin');
const cleanCSS = require('gulp-clean-css');
const uglify = require("gulp-uglify");
const autoprefixer = require('gulp-autoprefixer');
const server = require('gulp-server-livereload');
const rename = require("gulp-rename");
const minifyInline = require('gulp-minify-inline');

const production = !!util.env.production;


gulp.task('scss', () => {
	gulp.src('src/main.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(production? autoprefixer() : util.noop())
		.pipe(production ? cleanCSS() : util.noop())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest('dist'));
});

gulp.task('scss:watch', () => {
	gulp.watch('src/**/*.scss', ['scss']);
});

gulp.task('js', () => {
	gulp.src('src/app.js')
	.pipe(babel({
			presets: ['env']
		}))
		.pipe(production? uglify() : util.noop())
		.pipe(rename({suffix:'.min_v2'}))
		.pipe(gulp.dest('dist'));
	})
	
gulp.task('js:watch', () => {
	gulp.watch('src/*.js', ['js']);
})

gulp.task('html', () => {
	gulp.src('src/*.html')
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(production ? minifyInline() : util.noop())
		.pipe(gulp.dest('dist'));
})

gulp.task('html:watch', () => {
	gulp.watch('src/*.html', ['html']);
})

gulp.task('webserver', function () {
	gulp.src('dist')
		.pipe(server({
			livereload: true,
			directoryListing: false,
			open: true
		}));
});

gulp.task('default', ['scss', 'js', 'html', 'scss:watch', 'js:watch', 'html:watch', 'webserver']);